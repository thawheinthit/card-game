import React from 'react';

import { Platform, Dimensions, ActivityIndicator, StyleSheet } from "react-native";
import { hexColor } from '../config/colors';
import * as appInfo from "../app.json";
import { CARD_PAIRS_VALUE } from '../config/constants';

export const window = Dimensions.get('window');

export const hiddenIfAndroid = (displayObj) => {
    if (Platform.OS === "ios") return displayObj;
}



export const getLoader = <ActivityIndicator size="large" />

//KeyboardAvoidingView
export const screenBehavior = Platform.OS === 'ios' ? "padding" : "height"
export const keyboardVerticalOffset = Platform.OS === 'ios' ? 70 : 140





export const getAppVersion = () => {
  let number;
   if (Platform.OS === "android") {
    number = appInfo.expo.version;
  } else {
    number = appInfo.expo.version;
  }
  return number;
}


export const getRandomNo = () => Math.floor(Math.random() * 100);
export const shuffleArray = array => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
}

export const generateData = () => {
  let pairData = [];

  for (let index = 0; index < CARD_PAIRS_VALUE; index++) {
    const randomNo = getRandomNo();
    const isFound = pairData.find(item => item == randomNo);

    if(!isFound){
      pairData.push(randomNo)
    }
    continue
    
  }
  let finalData = [];
  const test = pairData.concat(pairData);
  finalData = shuffleArray(test).map((item, index) => {
    return {
      id : index + 1,
      value: item
    }
  })
  return finalData ;

}
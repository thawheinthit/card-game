import {
  ADD_TO_QUEUE, RESET_STEP, CLEAR_CURRENT_PAIR
} from "../action/types";
import { generateData } from "../../helper/util";


const initialState = {
    pairData: generateData(),
    solvedPairs: [],
    currentPair: [],
    step: 0,
    
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
    case ADD_TO_QUEUE:
        let currentSolved = state.solvedPairs;
          const currentValue = action.payload;
          const currentStep = state.step;
          let currentPair = state.currentPair;
          if(currentPair.length > 0){
            // there is value to compare

            // if only ONE value inside the array, add currentValue and compare two values inside the array
              // if same, add to solved array and remove from currentPair array
              // if not same, remove from currentPair array
            if(currentPair.length === 1){
                currentPair.push(currentValue)
                const toCompare = currentPair;
                if(toCompare[0].value === toCompare[1].value){
                  currentSolved.push(toCompare[0]);
                }
                // else{
                //   currentPair = [];
                // }
            }else{
                // if two values, remove the two values and add currentValue
                currentPair = [];
                currentPair.push(currentValue);
            }

          }else{
            // no value to compare, so add to array and remove if more than two value
            if(currentPair.length === 2){
              currentPair = [];
            }else{
              currentPair.push(currentValue)
            }
          }
          
          
        return {
          ...state,
          step: currentStep + 1,
          solvedPairs: currentSolved,
          currentPair: currentPair
        };
      case RESET_STEP:
        return{
          pairData: generateData(),
          solvedPairs: [],
          currentPair: [],
          step: 0
        }
      case CLEAR_CURRENT_PAIR: 
      return {
          ...state,
          currentPair: []
        };
      default:
        return state;
    }

}

export default reducer
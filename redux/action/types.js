
export const ADD_TO_QUEUE = 'ADD_TO_QUEUE'
export const REMOVE_FROM_QUEUE = 'REMOVE_FROM_QUEUE'
export const CLEAR_CURRENT_PAIR = 'CLEAR_CURRENT_PAIR'
export const RESET_STEP = 'RESET_STEP'

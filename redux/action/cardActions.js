
import {
   ADD_TO_QUEUE,
   REMOVE_FROM_QUEUE,
   RESET_STEP,
   CLEAR_CURRENT_PAIR

} from './types';

export const addToQueue = (cardInfo) => {
    return (dispatch) => {

        dispatch({ type: ADD_TO_QUEUE, payload: cardInfo })
        
    }
    
}


export const resetStep = () => {
    return { type: RESET_STEP };
}

export const clearCurrentPair = () => {
    return { type: CLEAR_CURRENT_PAIR };
}
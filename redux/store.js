import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducer';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// const store = createStore(
//     reducers,
//     {}, // default STATE of app
//     compose(applyMiddleware(thunk)),
// );

// const store = createStore(reducers, compose(applyMiddleware(thunk), Reactotron.createEnhancer()))
const store = createStore(
  reducers,
  /* preloadedState, */ composeEnhancers(compose(applyMiddleware(thunk)))
);
export default store;
import React , { useState }from "react";
import { View, Text, Image, TouchableHighlight, Animated } from "react-native";
import styles  from "./styles";
import { colors } from "../../config/colors";
import { useDispatch, useSelector } from "react-redux";
import * as cardActions from "../../redux/action/cardActions"
const CardItem = (props) => {
  const cardReducer = useSelector((state) => state.CardReducer);
  const dispatch = useDispatch();
    let animatedValue = new Animated.Value(0);
    let currentValue = 0;
  
    animatedValue.addListener(({ value }) => {
      currentValue = value;
      
    });

    const flipAnimation = () => {
      if (currentValue >= 90) {
        Animated.spring(animatedValue, {
          toValue: 0,
          tension: 10,
          friction: 8,
          useNativeDriver: false,
        }).start();
      } else {
        Animated.spring(animatedValue, {
          toValue: 180,
          tension: 10,
          friction: 8,
          useNativeDriver: false,
        }).start();
      }
    };

    const frontInterpolate = animatedValue.interpolate({
      inputRange: [0, 180],
      outputRange: ['0deg', '180deg'],
    });

    const backInterpolate = animatedValue.interpolate({
      inputRange: [0, 180],
      outputRange: ['180deg', '360deg'],
    });

    const frontAnimatedStyle = {
      transform: [{ rotateY: frontInterpolate }],
    };

    const backAnimatedStyle = {
      transform: [{ rotateY: backInterpolate }],
    };
    
    let value = props.isOpened ? props.data.value : "?"

    const onPressCard =  () => {
      // flipAnimation();
      props.onPress()
    }
    return (
      <TouchableHighlight onPress={onPressCard} underlayColor={colors.background}>
        <View style={[styles.row]}>
          <Animated.View style={[frontAnimatedStyle, styles.card]}>
            
            <View style={styles.container}>
              <Text style={styles.title}>
                {value}
              </Text>
            </View>
          </Animated.View>


          <Animated.View style={[backAnimatedStyle, styles.backCard]}>
            
            <View style={styles.container}>
              <Text style={styles.title}>
                {props.data.value}
              </Text>
            </View>
          </Animated.View>
        </View>
      </TouchableHighlight>
    );
}

export default CardItem;
import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../config/colors";
import * as _c from "../../config/constants";
const window = Dimensions.get('window');
const column = 3 ;


export default StyleSheet.create({
  row: {
    flex: 1,
    alignItems: "center",
    paddingHorizontal:5,
    paddingVertical: 5,
  },

  card: {
    width: (window.width / column) - 10,
    height: (window.width / column) + 40,
    borderRadius: 10,
    borderWidth: 4,
    borderColor: colors.pureWhite,
    backgroundColor: colors.primaryColor,
    shadowColor: colors.primaryText,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 5,
    backfaceVisibility: 'hidden',
    
  },

  backCard: {
     width: (window.width / column) - 10,
    height: (window.width / column) + 40,
    borderRadius: 10,
    position: "absolute",
    top: 0,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: colors.tabBarActive,
    backfaceVisibility: 'hidden'
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent:"center"
  },
  title: {
    marginTop:5,
    fontSize: _c.FONT_SIZES.XXL,
    color: colors.primaryText,
    alignSelf: "center",
    opacity: 0.8,
    color: colors.pureWhite
  },
});

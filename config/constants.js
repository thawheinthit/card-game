export const VERSION = "150";

export const XXL = 40;
export const XL = 20;
export const L = 18
export const M = 16;
export const S = 14;
export const XS = 10;

export const FONT_SIZES = {
  XXL,
  XL,
  L,
  M,
  S,
  XS
};

export const CARD_PAIRS_VALUE = 6
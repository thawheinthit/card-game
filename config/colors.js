export const colors = {
  primaryColor: "#2da4ef",
  background: "#403e43",
  screenBackground: "#f1f5f9",
  pureWhite: "#FFFFFF"
};
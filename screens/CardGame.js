import React , { useState }from "react";
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, FlatList, Button, Dimensions, Alert} from 'react-native';
import { CardItem } from "../components/CardItem"
import { useSelector, useDispatch } from "react-redux";
import * as _c from "../config/constants";
import * as cardActions from "../redux/action/cardActions"
import { colors } from "../config/colors";
const window = Dimensions.get('window');


export const CardGame = () => {
  const cardReducer = useSelector((state) => state.CardReducer);
  const cardData = cardReducer.pairData;
  const dispatch = useDispatch();

  const isOpenedCheck = (item) =>{
    if(cardReducer.solvedPairs.find(item1 => item1.value == item.value)){
      return true
    }else if(cardReducer.currentPair.find(item1 => item1.id == item.id))
    {
      return true
    }
    else{
      return false
    }
  }

  const handleOnPress = (item) => {

    if(!cardReducer.solvedPairs.find(item1 => item1.value == item.value)){
      dispatch(cardActions.addToQueue(item));
    }

    if(cardReducer.currentPair.length === 2){
      if(cardReducer.currentPair[0].value !== cardReducer.currentPair[1].value){
        setTimeout(() => {
          dispatch(cardActions.clearCurrentPair());
        }, 1000);
      }
    }

    if(cardReducer.solvedPairs.length === cardData.length/2){
      Alert.alert(
      "Congratulations!",
      "You win this game by " + cardReducer.step + " steps",
      [
       
        { text: "Try another round", onPress: () => dispatch(cardActions.resetStep()) }
      ]
    );
    }

  }
    
  return (
    <View style={styles.container}>
      
      <StatusBar style="auto" />
      <View style={styles.resultContainer}>
          <View style={styles.result}>
            <View style={styles.restartButton}>
                <Button
                  onPress={() => dispatch(cardActions.resetStep())}
                  title="Restart"
                  accessibilityLabel="Restart"
                  style={{color: colors.pureWhite}}
                  testID="App.Restart"
                />
            </View>
              
              <View style={styles.rightContainer}>
                  <Text style={styles.stepLabel} testID="App.Step">STEP: 
                  <Text style={styles.countLabel} testID="App.StepCount"> {cardReducer.step} </Text>
                  </Text>
                  
                </View>
          </View>
          
      </View>
      <FlatList
            style={styles.flatList} 
            data={cardData}
            renderItem={({ item }) => (
              <CardItem data={item} onPress={() => handleOnPress(item)} 
              isOpened = {isOpenedCheck(item)}
              testID="App.Card"
              />
            )}
            keyExtractor={(item) => item.id.toString()}
            numColumns={3}
            testID="App.CardList"
          />
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background,
    alignItems: 'center',
  },
  flatList:{
    width: window.width,
  },
  resultContainer:{
    marginTop: 60,
    width: window.width,
    height: 50,
    paddingHorizontal: 10,
  },
  result:{
    flexDirection: 'row',
  },
  rightContainer:{
    width: "80%",
    flexDirection: 'row',
    paddingTop:10
  },
  stepLabel:{
    flex:1,
    textAlign: "right",
    fontSize:_c.FONT_SIZES.L,
    color: colors.pureWhite
  },
  countLabel: {
    color: colors.primaryColor,
    fontSize: _c.FONT_SIZES.XL,
  }
});

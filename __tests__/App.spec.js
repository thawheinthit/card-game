import 'react-native';
import React from 'react';
import App from '../App';
import { render, fireEvent, act } from "@testing-library/react-native";
import { CardItem } from '../components/CardItem';
import { CardGame } from '../screens/CardGame';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import {generateData} from "../helper/util"

it('renders correctly', () => {
  const tree = renderer.create(<App />).toJSON();
  expect(tree).toMatchSnapshot();
});

describe('Cards', () => {
  it('should generate pairs using CARD_PAIRS_VALUE', () => {
        const cardData = generateData();
        expect(cardData.length).toBe(12)
  })
  it('renders a list of cards', () => {
   const { queryByTestId} = render(<App />);
   expect(queryByTestId("App.Card"))
  });
  it("should see STEP label", () => {
    const {getByTestId} = render(<App />);
        expect(getByTestId("App.Step"));
  })
});

describe('Reset', () => {
  it('should be clicked', () => {
   const {getByTestId, queryAllByText, getByText} = render(<App />);
   fireEvent.press(getByTestId("App.Restart"));
  });
});

import React , { useState }from "react";
import { Provider } from 'react-redux';
import store from './redux/store';
import { CardGame } from "./screens/CardGame";



export default function App() {
  
  return (
    <Provider store={store}>
      <CardGame />
    </Provider>
  );
}
